package Services;

import Domain.Position;

public class DistanceService {
	
	private static DistanceService single_instance = null;
	
	private DistanceService() {
		
	}
	
	public static DistanceService getInstance() {
        if (single_instance == null)
            single_instance = new DistanceService();
 
        return single_instance;
	}
	
	public boolean DistanceValidation(Position positionA, Position positionB, int maxDistance) {
		
		int x = Math.abs(positionB.X() - positionA.X());
		int y = Math.abs(positionB.Y() - positionA.Y());
		double distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
		
		if (distance <= maxDistance)
			return true;
		else
			return false;
	}

}
