package Services;

import  java.io.File;
import java.io.FileWriter;

public class FileSystemService {
	
	private static FileSystemService single_instance = null;
	
	String containerFolderPath = System.getProperty("user.dir") + "/Assets";
	String logFilePath = this.containerFolderPath + "/logFile.txt";
	
	File containerFolder;
	File logFile;
	
	private FileSystemService() {
		
		this.containerFolder = new File(containerFolderPath);
		this.logFile = new File(logFilePath);
	}
	
	public static FileSystemService getInstance() {
		
		if (single_instance == null)
			single_instance = new FileSystemService();
		
		return single_instance;
	}
	
	public void writeToLogFile(String text) {
		
		createLogFile();
		try {
			FileWriter writer = new FileWriter(logFile, true);
			writer.append(text + '\n');
			writer.close();
		} catch (Exception ex) {
			System.out.println("Cannot open log file:" + ex);
		}
	}
	
	private void createLogFile() {
		
		createContainerFolder();
		try {
			this.logFile.createNewFile();
		} catch(Exception ex) {
			System.out.println("Cannot create log file:" + ex);
		}
	}
	
	private void createContainerFolder() {
		
		if (!this.containerFolder.exists()) {
			this.containerFolder.mkdirs();
		}
	}
}
