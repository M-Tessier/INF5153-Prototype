package Services;

import java.util.Random;

public class DiceService {

	private static DiceService single_instance = null;
	
	private Random random;
	
	private DiceService() {
		this.random = new Random();
	}
	
	public static DiceService getInstance() {
        if (single_instance == null)
            single_instance = new DiceService();
 
        return single_instance;
	}
	
	public int Roll(int nbFaces, int nbDices) {
		
		int result = 0;
		for (int i = 0; i < nbDices; i++) {
			result += random.nextInt(nbFaces) + 1;
		}
		return result;
	}
}
