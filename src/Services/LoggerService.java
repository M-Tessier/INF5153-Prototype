package Services;

public class LoggerService {
	
	private static LoggerService single_instance = null;
	
	private FileSystemService fileSystemService;
	
	private LoggerService() {
		this.fileSystemService = FileSystemService.getInstance();
	}
	
	public static LoggerService getInstance() {
		if(single_instance == null)
			single_instance = new LoggerService();
		
		return single_instance;
	}
	
	public void log(String text) {
		fileSystemService.writeToLogFile(text);
		System.out.println(text);
	}
}
