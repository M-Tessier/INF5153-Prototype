package Domain;

public enum WeaponType {
	Dagger, ThrowingDagger, Axe, LongSword, Crossbow, Bow, Sheild, Mace
}
