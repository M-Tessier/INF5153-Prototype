/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 *
 * @author A-tar
 */
public class Arena extends IGame {
    
    public Arena(Character[] fighters) {
        super(fighters);
        beginFight();
    }
    
    private void beginFight() {
        Fight fight = new Fight(this.fighter);
        Fighter winner = fight.playMatch();
    }
    
}
