package Domain;


public abstract class Armor {
	
	protected ArmorType armorType;
	
	public abstract int getBaseAC(Character owner);
	
	public ArmorType getArmorType() {
		return this.armorType;
	}
}
