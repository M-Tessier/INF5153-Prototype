/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author A-tar
 */
public class BattleGround {
    Services.DiceService dice = Services.DiceService.getInstance();
    List<Position> positions;
    int width;
    int height;
    
    public BattleGround(Character[] fighter, int size) {
        width = height = size;
        positions = new ArrayList<Position>();
        placeFightersRandomly(fighter);
    }

    private void placeFightersRandomly(Character[] fighter) {
        for (int i = 0; i < fighter.length; i++) {
            if (fighter[i] == null) break;
            fighter[i].setPosition(dice.Roll(width, 1), dice.Roll(height, 1));
            positions.add(fighter[i].getPosition());
        }
    }
}
