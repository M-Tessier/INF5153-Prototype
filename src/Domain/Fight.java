/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author A-tar
 */
public class Fight {

    protected BattleGround battle;
    protected List<Character> turnOrder;
    private int currentFighter;
    private final Services.DiceService diceService = Services.DiceService.getInstance();

    public Fight(Character[] fighters) {
        battle = new BattleGround(fighters, 200);
        decideTurnOrder(fighters);
        currentFighter = 0;
        //Controller.getInstance().GameScreenController(turnOrder);
    }

    public Fighter playMatch() {
        while (turnOrder.size() > 1) {
            ITurn turn;
            Character fighter = turnOrder.get(currentFighter);

            if (fighter.isHuman) {
                turn = new HumanTurn(fighter, turnOrder.stream().filter(article -> !article.equals(fighter)).collect(Collectors.toList()));
            } else {
                turn = new AITurn(fighter);
            }

            turn.turn();

            if (++currentFighter >= turnOrder.size()) {
                currentFighter = 0;
            }
        }

        return null;
    }

    private void decideTurnOrder(Character[] fighters) {
        turnOrder = Arrays.asList(fighters);
        Collections.sort(turnOrder, (a, b) -> {
            int comp = (b.getInitiative())
                    -(a.getInitiative());
            return comp == 0 ? (diceService.Roll(2, 1) - 1) : comp;
        });
    }
}
