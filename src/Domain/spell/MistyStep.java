/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.spell;

import Domain.Position;

/**
 *
 * @author A-tar
 */
public class MistyStep extends ISpell{

    protected Position p;
    
    public MistyStep(Position _p) {
        super();
        this.level = 2;
        this.p = _p;
    }
    
    @Override
    public void cast() {
        if(this.distanceService.DistanceValidation(this.caster.getPosition(), p, 30)) {
            this.caster.setPosition(p.X(), p.Y());
            this.loggerService.log(this.caster.getName() + " casts misty step and telports to (" + p.X() + ", " + p.Y() + ")!");
        } else {
            this.loggerService.log("The destination is too far away!");
        }
    }
    
}
