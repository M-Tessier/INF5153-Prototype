/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.spell;

import Domain.Position;
import Domain.Character;
import Services.DiceService;
import Services.DistanceService;
import Services.LoggerService;

/**
 *
 * @author A-tar
 */
public abstract class ISpell {

    public Character target;
    public Character caster;
    public int damage;
    public int level = 0;
    protected DistanceService distanceService;
    protected DiceService diceService;
    protected LoggerService loggerService;
    protected int range = 60;
    protected int spellSaveDC;

    public ISpell() {
        this.distanceService = DistanceService.getInstance();
        this.diceService = DiceService.getInstance();
        this.loggerService = LoggerService.getInstance();
        this.spellSaveDC = 8 + this.caster.getProficiency() + this.caster.getIntelligence();
    }

    public boolean canHit(Position attackerPosition, Position defenderPosition) {

        if (distanceService.DistanceValidation(attackerPosition, defenderPosition, 60)) {
            return true;
        } else {
            return false;
        }
    }

    public abstract void cast();

}
