/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.spell;

import Domain.ArmorType;
import Domain.Character;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author A-tar
 */
public class FireBall extends AOESpell {

    public FireBall() {
        super();
        this.level = 3;
    }

    @Override
    public void cast() {
        for (Character next : this.getAOE(this.target)) {
            damage = this.diceService.Roll(6, 8);
            if (next.armor.getArmorType() == ArmorType.HeavyArmor
                    || this.diceService.Roll(20, 1) + target.getDexterity() < this.spellSaveDC) {
                next.receiveDamage(damage);
                this.loggerService.log(this.caster.getName() + " casts fire ball and successfully hit " + next.getName() + " and dealt " + damage + " points of damage!");
            } else {
                this.loggerService.log(next.getName() + " dodged the fire ball!");
            }
        }
    }

    private List<Character> getAOE(Character target) {
        List<Character> targets = new ArrayList<>();
        for (Character next : this.fighters) {
            if (this.distanceService.DistanceValidation(target.getPosition(), next.getPosition(), 20)) {
                targets.add(next);
            }

        }
        return targets;
    }
}
