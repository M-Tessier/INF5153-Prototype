/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.spell;

/**
 *
 * @author A-tar
 */
public class FireBolt extends ISpell{

    public FireBolt() {
        super();
        this.level = 0;
        this.damage = this.diceService.Roll(10, 2);
    }
    
    @Override
    public void cast() {
        if (canHit(this.target.getPosition(), this.caster.getPosition())) {
            int toHit = diceService.Roll(20, 1) + this.caster.getIntelligenceModifier() + this.caster.getProficiency();
            this.loggerService.log(this.caster.getName() + " rolls for an attack on " + this.target.getName() + " and gets " + toHit + " against an AC of " + this.target.getAC() + '.');
            if (toHit >= this.target.getAC() || this.target.isBinded) {
                this.target.receiveDamage(damage);
                this.loggerService.log(this.caster.getName() + " casts fire bolt and successfully hit " + this.target.getName() + " and dealt " + damage + " points of damage!");
            } else {
                this.loggerService.log(this.caster.getName() + " misses his fire bolt on " + this.target.getName() + '!');
            }
        } else {
            this.loggerService.log("The target is too fat away!");
        }
    }
    
}
