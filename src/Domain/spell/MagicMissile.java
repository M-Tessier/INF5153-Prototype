/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.spell;

/**
 *
 * @author A-tar
 */
public class MagicMissile extends ISpell {

    public MagicMissile() {
        super();
        this.level = 1;
        this.damage = this.diceService.Roll(4, 3) + 3;
    }

    @Override
    public void cast() {
        if (canHit(this.target.getPosition(), this.caster.getPosition())) {
            this.target.receiveDamage(damage);
            this.loggerService.log(this.caster.getName() + " successfully hit " + this.target.getName() + " with magic missiles and dealt " + damage + " points of damage!");
        } else {
            this.loggerService.log("The target is too fat away!");
        }
    }
}
