/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.spell;

import Domain.Wizard;

/**
 *
 * @author A-tar
 */
public class MageArmor extends ISpell{

    public MageArmor() {
        super();
        this.level = 1;
    }
    
    @Override
    public void cast() {
        ((Wizard)this.caster).armoured = true;
        this.loggerService.log(this.caster.getName() + " casts mage armor and reinforces his AC" + '!');
    }
    
}
