/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.spell;

import Domain.ArmorType;
import Domain.Position;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author A-tar
 */
public class LightningBolt extends AOESpell {

    public LightningBolt() {
        super();
        this.range = Integer.MAX_VALUE;
        this.level = 3;
    }

    @Override
    public void cast() {
        for (Domain.Character next : this.getAOE(this.target)) {
            damage = this.diceService.Roll(6, 8);
            if (next.armor.getArmorType() == ArmorType.HeavyArmor
                    || this.diceService.Roll(20, 1) + target.getDexterity() < this.spellSaveDC) {
                next.receiveDamage(damage);
                this.loggerService.log(this.caster.getName() + " casts lightning bolt and successfully hit " + next.getName() + " and dealt " + damage + " points of damage!");
            } else {
                this.loggerService.log(next.getName() + " dodged the lightning bolt!");
            }
        }
    }

    private List<Domain.Character> getAOE(Domain.Character target) {
        List<Domain.Character> targets = new ArrayList<>();
        Position p1 = target.getPosition();
        Position p2 = caster.getPosition();
        for (Domain.Character next : this.fighters) {
            if (onALine(p1, p2, next.getPosition())) {
                targets.add(next);
            }

        }
        return targets;
    }

    private boolean onALine(Position p1, Position p2, Position curr) {
        int dxc = curr.X() - p1.X();
        int dyc = curr.Y() - p1.Y();

        int dxl = p2.X() - p1.X();
        int dyl = p2.Y() - p1.Y();

        int cross = dxc * dyl - dyc * dxl;
        if (cross == 0)
            return true;
        return false;
    }
}
