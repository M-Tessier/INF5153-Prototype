/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain.spell;

/**
 *
 * @author A-tar
 */
public class HoldPerson extends ISpell {

    public HoldPerson() {
        super();
        this.level = 1;
    }

    @Override
    public void cast() {
        if (canHit(this.target.getPosition(), this.caster.getPosition())) {
            if (this.spellSaveDC > this.diceService.Roll(20, 1) + this.target.getWisdom()) {
                this.target.isBinded = true;
                this.loggerService.log(this.caster.getName() + " casts hold person on " + this.target.getName() + '!');
            } else {
                this.loggerService.log(this.caster.getName() + " misses hold person on " + this.target.getName() + '!');
            }
        } else {
            this.loggerService.log("The target is too fat away!");
        }
    }

}
