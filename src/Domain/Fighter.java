package Domain;

public class Fighter extends Character {

	public Fighter(String name, int level, int maxHP, int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma) {
		super(name, level, maxHP, strength, dexterity, constitution, intelligence, wisdom, charisma);
	}
	
	public boolean equipArmor(Armor armor) {
		if (armor.getArmorType() == ArmorType.MediumArmor || armor.getArmorType() == ArmorType.HeavyArmor) {
			this.loggerService.log(this.name + " equipped " + armor.getArmorType().name() + '.');
			this.armor = armor;
			return true;
		} 
		else {
			this.loggerService.log(this.name + ", as a fighter, can only equip Medium or Heavy Armors.");
			return false;
		}
	}
	
	public void levelUp (int strengthBonus, int dexterityBonus, int constitutionBonus, int intelligenceBonus, int wisdomBonus, int charismaBonus) {
		this.incrementLevel();
		this.applyMaxHPBonus(constitutionBonus, this.hitDiceRoll());
		this.applyStrengthBonus(strengthBonus);
		this.applyDexterityBonus(dexterityBonus);
		this.applyConstitutionBonus(constitutionBonus);
		this.applyIntelligenceBonus(intelligenceBonus);
		this.applyWisdomBonus(wisdomBonus);
		this.applyCharismaBonus(charismaBonus);
		this.receiveHeal(diceService.Roll(4, 4) + 4);
	}
	
	public int hitDiceRoll() {
		return this.diceService.Roll(10, 1);
	}
}
