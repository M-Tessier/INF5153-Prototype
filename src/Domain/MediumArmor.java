package Domain;

public class MediumArmor extends Armor{

	public MediumArmor() {
		this.armorType = ArmorType.MediumArmor;
	}
	
	public int getBaseAC(Character owner) {
		return 13 + owner.getDexterityModifier();
	}
}
