package Domain;

import java.util.ArrayList;

public class Rogue extends Character{

        public boolean hasDoubleDagger;
	public Rogue(String name, int level, int maxHP, int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma) {
		super(name, level, maxHP, strength, dexterity, constitution, intelligence, wisdom, charisma);
                hasDoubleDagger = false;
	}
	
	public boolean equipArmor(Armor armor) {
		if (armor.getArmorType() == ArmorType.NoArmor || armor.getArmorType() == ArmorType.LightArmor) {
			this.loggerService.log(this.name + " equipped " + armor.getArmorType().name() + '.');
			this.armor = armor;
			return true;
		} 
		else {
			this.loggerService.log(this.name + ", as a rogue, can only equip Light Armors.");
			return false;
		}
	}
	
	public boolean equipWeapon(Weapon weapon) {
                this.hasDoubleDagger = false;
		return this.equipWeapon(weapon, null);
	}
	
	public boolean equipWeapon(Weapon weapon1, Weapon weapon2) {

		ArrayList<Weapon> weapons = new ArrayList<Weapon>();
		
		if ((weapon1.getWeaponType() == WeaponType.Dagger || weapon1.getWeaponType() == WeaponType.ThrowingDagger || weapon1.getWeaponType() == WeaponType.Mace) &&
				(weapon2 == null || weapon2.getWeaponType() == WeaponType.Dagger || weapon2.getWeaponType() == WeaponType.ThrowingDagger || weapon2.getWeaponType() == WeaponType.Mace))
		{
                    this.hasDoubleDagger = false;
			if (weapon2 == null) {
				weapons.add(weapon1);
				this.loggerService.log(this.name + " equipped " + weapon1.getWeaponType().name() + '.');
			}
			else {
				weapons.add(weapon1);
				weapons.add(weapon2);
				this.loggerService.log(this.name + " equipped " + weapon1.getWeaponType().name() + " and the " + weapon2.getWeaponType().name() + '.');
			}
			
			this.setEquippedWeapons(weapons);
                        if (weapon1.getWeaponType() == WeaponType.Dagger && weapon2 != null && weapon2.getWeaponType() == WeaponType.Dagger )
                            this.hasDoubleDagger = true;
			return true;
		}
		else {
			this.loggerService.log(this.name + ", as a wizard, can only equip Daggers or Maces.");
			return false;
		}
	}
	
	public void levelUp (int strengthBonus, int dexterityBonus, int constitutionBonus, int intelligenceBonus, int wisdomBonus, int charismaBonus) {
		this.incrementLevel();
		this.applyMaxHPBonus(constitutionBonus, this.hitDiceRoll());
		this.applyStrengthBonus(strengthBonus);
		this.applyDexterityBonus(dexterityBonus);
		this.applyConstitutionBonus(constitutionBonus);
		this.applyIntelligenceBonus(intelligenceBonus);
		this.applyWisdomBonus(wisdomBonus);
		this.applyCharismaBonus(charismaBonus);
		this.receiveHeal(diceService.Roll(4, 4) + 4);
	}
	
	public int hitDiceRoll() {
		return this.diceService.Roll(8, 1);
	}
        
        public boolean critAttack(Character defender, Weapon weapon) {
                int preHitHP = defender.getCurrentHP();
		boolean hasHit = this.attack(defender, weapon);
                if (!hasHit || preHitHP == defender.getCurrentHP())
                    return hasHit;
                int extraDamage = 0;
                if(this.level >= 1) {
                    extraDamage = this.diceService.Roll(6, 1);
                } else if(this.level >= 1) {
                    extraDamage = this.diceService.Roll(6, 3);
                } else if(this.level >= 1) {
                    extraDamage = this.diceService.Roll(6, 5);
                } else if(this.level >= 1) {
                    extraDamage = this.diceService.Roll(6, 7);
                }
                defender.receiveDamage(extraDamage);
                return hasHit;
	}
}
