package Domain;

public class ThrowingDagger extends Weapon{

	public ThrowingDagger() {
		super();
		this.weaponType = WeaponType.ThrowingDagger;
		this.isTwoHanded = false;
		this.range = 20;
	}
	
	public int getDamage(Character owner, boolean critical) {
		int value = this.diceService.Roll(4, 1);
		if (critical)
			value *= 2;
		
		return value + owner.getDexterityModifier();
	}
}
