package Domain;

public class LongSword extends Weapon{

	
	public WeaponType weaponType = WeaponType.LongSword;

	public LongSword() {
		super();
		this.isTwoHanded = false;
		this.range = 5;
	}
	
	public int getDamage(Character owner, boolean critical) {
		int value;
		if (this.isTwoHanded)
			value = this.diceService.Roll(10, 1);
		else
			value = this.diceService.Roll(8, 1);
		if (critical)
			value *= 2;
		
		return value + owner.getStrengthModifier();
	}
}
