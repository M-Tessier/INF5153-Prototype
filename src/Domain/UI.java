/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

/**
 *
 * @author A-tar
 */
public abstract class UI {

    public abstract void display();

    public abstract String getInput();

    public void clearInterface() {
        //System.out.print("\033[H\033[2J");  
        //System.out.flush(); 
    }
}
