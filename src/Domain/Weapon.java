package Domain;

import Services.DistanceService;
import Services.DiceService;;

public abstract class Weapon {
	
	public WeaponType weaponType;
	
	protected boolean isTwoHanded;
	protected int range;
	
	protected DistanceService distanceService;
	protected DiceService diceService;
	
	public Weapon() {
		this.distanceService = DistanceService.getInstance();
		this.diceService = DiceService.getInstance();
	}
	
	public abstract int getDamage(Character owner, boolean critical);
	
	public boolean canHit(Position attackerPosition, Position defenderPosition) {
		
		if (distanceService.DistanceValidation(attackerPosition, defenderPosition, this.range)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isTwoHanded() {
		return this.isTwoHanded;
	}
	
	public WeaponType getWeaponType() {
		return this.weaponType;
	}
}
