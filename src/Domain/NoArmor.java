package Domain;

public class NoArmor extends Armor{
	
	public NoArmor() {
		this.armorType = ArmorType.NoArmor;
	}
	
	public int getBaseAC(Character owner) {
		return 10 + owner.getDexterityModifier();
	}
}
