package Domain;

public class Position {
	private int X;
	private int Y;
	
	public Position(int x, int y) {
		this.X = x;
		this.Y = y;
	}
	
	public int X() {
		return this.X;
	}
	
	public int Y() {
		return this.Y;
	}
        
        @Override
        public boolean equals(Object p) {
            return ((Position)p).X() == this.X && ((Position)p).Y() == this.Y;
        }
}
