package Domain;

import org.json.*;
import Services.DiceService;
import Services.JSONUtil;

import java.util.Scanner;

public class Main {
	
	// Instantiation of weapons and armors
	final static Weapon axe = new Axe();
	final static Weapon bow = new Bow();
	final static Weapon crossbow = new Crossbow();
	final static Weapon dagger = new Dagger();
	final static Weapon longSword = new LongSword();
	final static Weapon mace = new Mace();
	final static Weapon shield = new Sheild();
	final static Weapon throwingDagger = new ThrowingDagger();
	
	final static Armor heavyArmor = new HeavyArmor();
	final static Armor lightArmor = new LightArmor();
	final static Armor mediumArmor = new MediumArmor();
	final static Armor noArmor = new NoArmor();
	
	public static void main(String[] args) {
		
		int nbCHaracterPerGame = 16;
		DiceService diceService = DiceService.getInstance();
		Character [] prefabList = new Character[nbCHaracterPerGame];
		Character [] characterList = new Character[nbCHaracterPerGame];

		Scanner in = new Scanner(System.in);
		int charChoice = 0, rep = 0, repGameType = 0, nbCharacter = 0, repPrefab = 0, repRepickPrefab =0;

		Character prefab = null, characterInGame = null;
		
		//IMPORT prefabs
		JSONObject obj = JSONUtil.getJSONObjectFromFile("/data/character.json");
		
		JSONArray jsonArray = obj.getJSONArray("Array");
		Object prefabChar = null;
		
		for(int i = 0; i < jsonArray.length(); i++) {
			//grab character stats
			prefabChar =  jsonArray.get(i);
			int str = ((JSONObject) prefabChar).getInt("str");
			int dex = ((JSONObject) prefabChar).getInt("dex");
			int con = ((JSONObject) prefabChar).getInt("con");
			int wis = ((JSONObject) prefabChar).getInt("wis");
			int level = ((JSONObject) prefabChar).getInt("level");
			int cha = ((JSONObject) prefabChar).getInt("cha");
			int intel = ((JSONObject) prefabChar).getInt("int");
			String classChar = ((JSONObject) prefabChar).getString("class");
			String nameChar = ((JSONObject) prefabChar).getString("name");
			
			//building prefab
			int maxHP =  2 + diceService.Roll(8,1) + con;

			if(classChar.equalsIgnoreCase("Rogue")) {
				prefab = new Rogue(nameChar, level, maxHP,str, dex, con, intel,wis,cha);
			}else if(classChar.equalsIgnoreCase("Fighter")) {
				prefab = new Fighter(nameChar, level, maxHP,str, dex, con, intel,wis,cha);
			}else if(classChar.equalsIgnoreCase("Wizard")) {
				prefab = new Wizard(nameChar, level, maxHP,str, dex, con, intel,wis,cha);
			}			
			prefabList[i] = prefab;			
		}
		

		//Game Start
		System.out.println("Welcome to our D&D prototype\nWould you like to create your own character or choose from a list of prefabs ?\n1-Create\n2-Prefab");
		
		while(charChoice != 1 && charChoice != 2) {
			if(in.hasNextInt()) {
				charChoice = in.nextInt();
				if(charChoice < 1 || charChoice > 2) {
					System.out.println("Enter 1 or 2 please");
				}
			}else {
				System.out.println("Enter 1 or 2 please");
				in.next();
				continue;
			}
		}
		
		// player creation
		if(charChoice == 1) {
			do {
				Character character = Creation.creationChar();			

				characterList[nbCharacter] = character;
				nbCharacter++;
				
				System.out.println("Do you want to add another character of your creation?\n1-Yes\n2-No");			
				do {
					if(in.hasNextInt()) {
						rep = in.nextInt();
						if(rep < 1 || rep > 2) {
							System.out.println("Enter a number between 1 or 2");
						}
					}else {
						System.out.println("Enter a number between 1 or 2");
						in.next();
						continue;
					}
				}while(rep != 1 && rep!= 2);

			}while(rep == 1 && nbCharacter < 16);
			
		//player choosing from prefabs
		}else if(charChoice == 2) {
			do {
				System.out.println("Choose between these characters :");
				for(int i = 0; i < nbCHaracterPerGame; i++) {
					System.out.println((i+1) +"-" +prefabList[i].name + " str : " + prefabList[i].strength + " dex : " + prefabList[i].dexterity + " con : " + prefabList[i].constitution
							+ " int : " + prefabList[i].intelligence + " wis : " + prefabList[i].wisdom + " cha : " + prefabList[i].charisma + " maxHP : " + prefabList[i].maxHP);
				}
				System.out.println();
				
				do {
					if(in.hasNextInt()) {
						repPrefab = in.nextInt();
						if(repPrefab < 1 || repPrefab > 16) {
							System.out.println("Enter a number between 1 and 16");
						}
					}else {
						System.out.println("Enter a number between 1 and 16");
						in.next();
						continue;
					}
				}while(repPrefab < 1 || repPrefab > 16);
				
				characterList[nbCharacter] = prefabList[repPrefab - 1];
				nbCharacter++;
				
				System.out.println("Would you like to add another prefab ?\n1-Yes\n2-No");
				do {
					if(in.hasNextInt()) {
						repRepickPrefab = in.nextInt();
						if(repRepickPrefab < 1 || repRepickPrefab > 2) {
							System.out.println("Enter a number between 1 or 2");
						}
					}else {
						System.out.println("Enter a number between 1 or 2");
						in.next();
						continue;
					}
				}while(repRepickPrefab != 1 && repRepickPrefab!= 2);
			}while(repRepickPrefab == 1);

		}		
		
		//filling the game with prefabs if needed
		for(int i = nbCharacter; i<nbCHaracterPerGame;i++ ) {
			characterList[i] = prefabList[i];
		}
		
		//equiping characters
		for(int i = 0; i<nbCHaracterPerGame;i++ ) {
			characterInGame = characterList[i];
			if(characterInGame instanceof Rogue) {
				characterInGame.equipArmor(lightArmor);
				characterInGame.weapons.add(dagger);
				characterInGame.weapons.add(mace);
				characterInGame.equipWeapon(dagger);
				characterInGame.weapons.add(throwingDagger);					
			}else if(characterInGame instanceof Wizard){
				characterInGame.equipArmor(lightArmor);
				characterInGame.weapons.add(dagger);
				characterInGame.weapons.add(mace);
				characterInGame.equipWeapon(dagger);
			}else if(characterInGame instanceof Fighter){
				characterInGame.equipArmor(heavyArmor);
				characterInGame.weapons.add(dagger);
				characterInGame.weapons.add(mace);
				characterInGame.weapons.add(throwingDagger);
				characterInGame.weapons.add(longSword);
				characterInGame.weapons.add(shield);
				characterInGame.weapons.add(bow);
				characterInGame.weapons.add(crossbow);
				characterInGame.weapons.add(axe);
				characterInGame.equipWeapon(axe);
			}
		}
		
		System.out.println("\nWhat kind of game would you like to play ?\n1-Tournament\n2-Arena\n");
		do {
			if(in.hasNextInt()) {
				repGameType = in.nextInt();
				if(repGameType < 1 || repGameType > 2) {
					System.out.println("Enter a number between 1 or 2");
				}
			}else {
				System.out.println("Enter a number between 1 or 2");
				in.next();
				continue;
			}
		}while(repGameType != 1 && repGameType!= 2);
                IGame game;
		if(repGameType == 1) {
                    game = new Tournement(trimArray(characterList));
		}else if(repGameType == 2) {
                    game = new Arena(trimArray(characterList));
		}
		
		
		in.close();
		

	}
        
        private static Character[] trimArray(Character[] chars) {
            int count = 0;
            for (Character c: chars) {
                if(c==null) break;
                count++;
            }
            Character[] ret = new Character[count];
            count = 0;
            for (Character c: chars) {
                if(c==null) break;
                ret[count] = c;
                count++;
            }
            return ret;
        }
}
