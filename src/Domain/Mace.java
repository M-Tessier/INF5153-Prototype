package Domain;

public class Mace extends Weapon{

	public Mace() {
		super();
		this.weaponType = WeaponType.Mace;
		this.isTwoHanded = false;
		this.range = 5;
	}
	
	public int getDamage(Character owner, boolean critical) {
		
		int value = this.diceService.Roll(6, 1);
		if (critical)
			value *= 2;
		
		return value + owner.getStrengthModifier();
	}
}
