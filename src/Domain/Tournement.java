/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author A-tar
 */
public class Tournement extends IGame {
    
    public List<Character> bracket;
    private int numsBracket;
    private int currentBracket;

    public Tournement(Character[] _fighter) {
        super(_fighter);
        bracket = new ArrayList<Character>(Arrays.asList(this.fighter));
        numsBracket = (this.fighter.length / 2);
        currentBracket = 0;
        beginTournement();
    }
    
    private void beginTournement() {
        while(this.bracket.size() >= 2) {
            Character[] currentPlayers = getBracket(bracket, currentBracket);
            Fight fight = new Fight(currentPlayers);
            Fighter winner = fight.playMatch();
            int looser = currentPlayers[0].name.equals(winner.name) ? 1 : 0;
            bracket.remove(bracket.indexOf(currentPlayers[looser]));
            if (++currentBracket >= bracket.size()) currentBracket = 0;
        }
    }
    
    private Character[] getBracket(List<Character> list, int curr) {
        return list.subList(curr, curr + 2).toArray(new Character[2]);
    }
    
}
