package Domain;

public class Bow extends Weapon{

	public Bow() {
		super();
		this.weaponType = WeaponType.Bow;
		this.isTwoHanded = true;
		this.range = 100;
	}
	
	public int getDamage(Character owner, boolean critical) {
		int value = this.diceService.Roll(8, 1);
		if (critical)
			value *= 2;
		
		return value + owner.getDexterityModifier();
	}
}
