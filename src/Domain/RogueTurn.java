/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import Services.DistanceService;
import java.util.List;

/**
 *
 * @author A-tar
 */
public class RogueTurn extends HumanTurn {

    private int movementPoint;
    private int attackPoint;
    private boolean canCrit;

    public RogueTurn(Character _fighter, List<Character> enemies) {
        super(_fighter, enemies);
        this.actionPoint = _fighter.level >= 3 ? 3 : 2;
        this.movementPoint = _fighter.level >= 3 ? 2 : 1;
    }

    @Override
    protected boolean move(Position p) {
        if (movementPoint == 0) {
            return false;
        }
        return super.move(p);
    }

    @Override
    protected boolean attack(Character target) {
        if ((attackPoint == 0 && !((Rogue) fighter).hasDoubleDagger)
                || (attackPoint == -1 && ((Rogue) fighter).hasDoubleDagger)) {
            return false;
        }
        for (Character e : enemies) {
            if (DistanceService.getInstance().
                    DistanceValidation(target.getPosition(), e.getPosition(), target.equippedWeapons.get(0).range)) {
                canCrit = false;
                return ((Rogue) fighter).critAttack(target, fighter.equippedWeapons.get(0));

            }
        }
        return super.attack(target);
    }

}
