/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.List;

/**
 *
 * @author A-tar
 */
public class FighterTurn extends HumanTurn{
    
    private boolean doubleAttack;
    
    public FighterTurn(Character _fighter, List<Character> enemies) {
        super(_fighter, enemies);
        if(this.fighter.level >= 5) doubleAttack = true;
        actionPoint = 2;
    }

    @Override
    protected boolean attack(Character target) {
        if(doubleAttack)
            super.attack(target);
        return super.attack(target);
    }
    
}
