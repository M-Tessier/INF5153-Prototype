package Domain;

public class HeavyArmor extends Armor{
	
	public HeavyArmor() {
		this.armorType = ArmorType.HeavyArmor;
	}

	public int getBaseAC(Character owner) {
		if (owner.getStrength() >= 15)
			return 18;
		else if (owner.getStrength() == 13 || owner.getStrength() == 14)
			return 16;
		else
			return 14;
	}
}
