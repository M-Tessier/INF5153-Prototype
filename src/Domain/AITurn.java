/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import Services.DiceService;

/**
 *
 * @author A-tar
 */
public class AITurn extends ITurn {

    protected int actionPoint;

    public AITurn(Character _fighter) {
        super(_fighter);
        actionPoint = 2;
    }

    @Override
    public void turn() {
        DiceService dice = DiceService.getInstance();
        while (actionPoint > 0) {
            int x, y;
            do {
                x = this.fighter.getPosition().X() + (dice.Roll(30, 1) - 15);
                y = this.fighter.getPosition().Y() + (dice.Roll(30, 1) - 15);
            } while (x > 200 || y > 200 && x < 0 || y < 0);
            if (move(new Position(x, y))) {
                actionPoint--;
            }
        }
    }

    protected boolean move(Position p) {
        return this.fighter.move(p);
    }

}
