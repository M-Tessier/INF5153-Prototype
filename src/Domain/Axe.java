package Domain;

public class Axe extends Weapon{

	public Axe() {
		super();
		this.weaponType = WeaponType.Axe;
		this.isTwoHanded = false;
		this.range = 5;
	}
	
	public int getDamage(Character owner, boolean critical) {
		
		int value = this.diceService.Roll(10, 1);
		if (critical)
			value *= 2;
		
		return value + owner.getStrengthModifier();
	}
}
