package Domain;

public class Crossbow extends Weapon{

	public Crossbow() {
		super();
		this.weaponType = WeaponType.Crossbow;
		this.isTwoHanded = true;
		this.range = 100;
	}
	
	public int getDamage(Character owner, boolean critical) {
		int value = this.diceService.Roll(10, 1);
		if (critical)
			value *= 2;
		
		return value + owner.getDexterityModifier();
	}
}
