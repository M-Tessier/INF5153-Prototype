package Domain;

public class LightArmor extends Armor {

	public LightArmor() {
		this.armorType = ArmorType.LightArmor;
	}
	
	public int getBaseAC(Character owner) {
		return 11 + owner.getDexterityModifier();
	}
}
