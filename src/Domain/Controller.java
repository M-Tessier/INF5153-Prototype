/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.List;
import Domain.spell.FireBall;
import Domain.spell.FireBolt;
import Domain.spell.ISpell;
import Domain.spell.MageArmor;
import Domain.spell.MagicMissile;
import Domain.spell.HoldPerson;
import Domain.spell.LightningBolt;
import Domain.spell.MistyStep;

/**
 *
 * @author A-tar
 */
public class Controller {

    private final MainMenu_UI mainMenu;
    private final FighterUI fighterUI;
    private final GameScreen gameScreen;
    private final GameSettingsUI settingUI;
    private static Controller single_instance = null;

    public Controller() {
        mainMenu = new MainMenu_UI();
        fighterUI = new FighterUI();
        gameScreen = new GameScreen();
        settingUI = new GameSettingsUI();
    }

    public static Controller getInstance() {
        if (single_instance == null) {
            single_instance = new Controller();
        }

        return single_instance;
    }

    public void GameSettingsController() {

    }

    public void GameScreenController(Character currentFighter, List<Character> c, int actionPoint) {
        gameScreen.updateState(currentFighter, c, actionPoint);
        gameScreen.display();
    }

    public void FighterController() {
        fighterUI.display();
    }

    public void MainMenuController() {
        mainMenu.display();

        String input = "";
        while (!valid(input)) {
            input = mainMenu.getInput();
        }
        switch (input) {
            case "1":
                FighterController();
                break;
            case "2":
                System.out.println("2");
                break;
        }
    }

    public boolean valid(String s) {
        return false;
    }

    public HumanTurn.Action getNextAction() {
        gameScreen.displayChoice();
        String input = "";
        while (!"1".equals(input) && !"2".equals(input) && !"3".equals(input) && !"4".equals(input)) {
            input = gameScreen.getInput();
        }
        switch (input) {
            case "1":
                return HumanTurn.Action.MOVE;
            case "2":
                gameScreen.displayAttackChoice();
                input = "";
                while (!"1".equals(input) && !"2".equals(input)) {
                    input = gameScreen.getInput();
                }
                return input.equals("1") ? HumanTurn.Action.ATTACK : HumanTurn.Action.CAST_SPELL;
            case "3":
                return HumanTurn.Action.CHANGE_WEAPON;
            case "4":
                return HumanTurn.Action.END_TURN;
        }
        return HumanTurn.Action.END_TURN;
    }

    public Position querryPosition() {
        gameScreen.displayMove();
        int myIntValue;
        int x = 0, y = 0;
        boolean areNums = false;
        while (!areNums) {
            String input = gameScreen.getInput();
            String[] nums = input.split(" ");
            try {
                x = Integer.parseInt(nums[0]);
                y = Integer.parseInt(nums[1]);
                areNums = true;
            } catch (Exception e) {

            }
        }

        return new Position(x, y);
    }

    public ISpell querrySpell(Character c) {
        gameScreen.displaySpells();
        int myIntValue;
        int x = 0;
        boolean isNum = false;
        while (!isNum || !(x <= 7 && x > 0)) {
            String s = gameScreen.getInput();
            try {
                x = Integer.parseInt(s);
                isNum = true;
            } catch (NumberFormatException e) {
                isNum = false;
            }
        }
        ISpell spell = null;
        switch (x) {
            case 1:
                spell = new FireBolt();
                spell.target = querryTarget(c);
                break;
            case 2:
                spell = new MagicMissile();
                spell.target = querryTarget(c);
                break;
            case 3:
                spell = new MageArmor();
                break;
            case 4:
                spell = new MistyStep(querryPosition());
                break;
            case 5:
                spell = new HoldPerson();
                spell.target = querryTarget(c);
                break;
            case 6:
                spell = new FireBall();
                spell.target = querryTarget(c);
                break;
            case 7:
                spell =  new LightningBolt();
                spell.target = querryTarget(c);
                break;
        }
        return spell;
    }
    
    public Weapon querryWeapon() {
        gameScreen.displayWeapons();
        int x = 0;
        boolean isNum = false;
        while (!isNum || !(x <= gameScreen.currentFighter.weapons.size() && x > 0)) {
            String s = gameScreen.getInput();
            try {
                x = Integer.parseInt(s);
                isNum = true;
            } catch (NumberFormatException e) {
                isNum = false;
            }
        }
        return this.gameScreen.currentFighter.weapons.get(x - 1);
        
    }
    
    public Weapon[] querryWeapons() {
        Weapon[] w = new Weapon[2];
        w[0] = querryWeapon();     
        gameScreen.displayMultipleWeapon();
        String y = "";
        while (!y.equals("1") && !y.equals("2"))
            y = gameScreen.getInput();
        if(y.equals("2")) {
            return w;
        } else {
            w[1] = querryWeapon();
            return w;
        }
    }

    public Character querryTarget(Character player) {
        gameScreen.displayTargets(player);
        int x = 0;
        boolean isNum = false;
        while (!isNum || !(x <= gameScreen.fighters.size() && x > 0)) {
            String s = gameScreen.getInput();
            try {
                x = Integer.parseInt(s);
                isNum = true;
            } catch (NumberFormatException e) {
                isNum = false;
            }
        }
        return this.gameScreen.fighters.get(x - 1);
    }

    void displayCantSpell() {
        gameScreen.displayImpossible();
    }
}
