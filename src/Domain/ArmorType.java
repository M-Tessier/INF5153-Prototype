package Domain;

public enum ArmorType {
	NoArmor, LightArmor, MediumArmor, HeavyArmor
}
