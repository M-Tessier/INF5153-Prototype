package Domain;

import Services.LoggerService;

import java.util.ArrayList;

import Services.DiceService;
import Services.DistanceService;

public abstract class Character {
	
	protected String name;
        
        protected boolean isHuman = true;
        
        public boolean isBinded;
	
	protected int level;
        public int experience;
	
	protected int maxHP;
	protected int currentHP;
	
	protected int strength;
	protected int dexterity;
	protected int constitution;
	protected int intelligence;
	protected int wisdom;
	protected int charisma;
        protected int initiative;
	
	protected ArrayList<Weapon> weapons;
	protected ArrayList<Weapon> equippedWeapons;
	
	public Armor armor;
	
	protected Position position;
	
	protected LoggerService loggerService;
	protected DiceService diceService;
	protected DistanceService distanceService;
	
	public Character(String name, int level, int maxHP, int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma) {
		
		this.loggerService = LoggerService.getInstance();
		this.diceService = DiceService.getInstance();
		this.distanceService = DistanceService.getInstance();
		
		this.name = name;
		this.level = level;
                this.experience = 0;
		this.maxHP = maxHP;
		this.currentHP = maxHP;
		this.strength = strength;
		this.dexterity = dexterity;
		this.constitution = constitution;
		this.intelligence = intelligence;
		this.wisdom = wisdom;
		this.charisma = charisma;
                this.initiative = this.dexterity + this.diceService.Roll(20, 1);
                
                this.isBinded = false;
		
		this.weapons = new ArrayList<Weapon>();
		this.equippedWeapons = new ArrayList<Weapon>();
		this.armor = new NoArmor();
	}
	
	public Position getPosition() {
		
		return this.position;
	}
        
        public int getInitiative() {
            return this.initiative;
        }
	
	public void setPosition(int x, int y) {
		this.position = new Position(x, y);
                this.loggerService.log(this.name + " was placed at (" + x + ", " + y + ")");
	}
	
	public boolean move(Position destination) {
		
		if (distanceService.DistanceValidation(this.position, destination, 30)) {
			this.position = destination;
			this.loggerService.log(this.name + " moves to (" + this.getPosition().X() + ", " + this.getPosition().Y() + ").");
			return true;
		} else {
			this.loggerService.log(this.name + " cannot move to (" + destination.X() + ", " + destination.Y() + "), it's too far!");
			return false;
		}
	}
	
	public boolean attack(Character defender, Weapon weapon) {
		
		if (weapon.canHit(this.position, defender.position)) {
			int toHit = diceService.Roll(20, 1) + this.getStrengthModifier() + this.getProficiency();
			this.loggerService.log(this.name + " rolls for an attack on " + defender.getName() + " and gets " + toHit + " against an AC of " + defender.getAC() + '.');
			if (toHit >= defender.getAC() || defender.isBinded) {
				
				int damage;
				if (toHit == 20)
					damage = weapon.getDamage(this, true);
				else
					damage = weapon.getDamage(this, false);
				
				defender.receiveDamage(damage);
                                if (defender.currentHP <= 0)
                                    if(++experience == 2) {
                                        experience = 0;
                                        this.levelUp(0, 0, 0, 0, 0, 0);
                                    }
				this.loggerService.log(this.name + " successfully hit " + defender.getName() + " and dealt " + damage + " points of damage!");
			} else {
				this.loggerService.log(this.name + " misses his attack on " + defender.getName() + '!');
			}
			return true;
				
		} else {
			this.loggerService.log(this.name + " cannot attack " + defender.getName() + " at (" + defender.getPosition().X() + ", " + defender.getPosition().Y() + "), it's too far!");
			return false;
		}
	}
	
	public void receiveDamage(int damage) {
		this.currentHP -= damage;
		this.loggerService.log(this.name + " is damaged: " + (this.getCurrentHP() + damage) + '/' + this.getMaxHP() + " => " + this.getCurrentHP() + '/' + this.getMaxHP() + '.');
	}
	
	public void receiveHeal(int heal) {
		if (this.currentHP + heal > maxHP)
			this.currentHP = maxHP;
		else
			this.currentHP += heal;
		this.loggerService.log(this.name + " is healed: " + (this.getCurrentHP() - heal) + '/' + this.getMaxHP() + " => " + this.getCurrentHP() + '/' + this.getMaxHP() + '.');
	}
	
	public int getAC() {
		
		int value = this.armor.getBaseAC(this);
		
		for (Weapon weapon : weapons) {
			if (weapon.getWeaponType() == WeaponType.Sheild)
				value += 2;
		}
			
		return value;
	}
	
	public boolean equipWeapon(Weapon weapon) {
		return this.equipWeapon(weapon, null);
	}
	
	public boolean equipWeapon(Weapon weapon1, Weapon weapon2) {

		ArrayList<Weapon> weapons = new ArrayList<Weapon>();
		
		if (weapon2 == null) {
			weapons.add(weapon1);
			this.loggerService.log(this.name + " equipped " + weapon1.getWeaponType().name() + '.');
		}
		else {
			weapons.add(weapon1);
			weapons.add(weapon2);
			this.loggerService.log(this.name + " equipped " + weapon1.getWeaponType().name() + " and the " + weapon2.getWeaponType().name() + '.');
		}
		
		this.setEquippedWeapons(weapons);
		return true;
	}
	
	public boolean equipArmor(Armor armor) {
		this.armor = armor;
		this.loggerService.log(this.name + " equipped " + armor.getArmorType().name() + '.');
		return true;
	}
	
	public abstract void levelUp(int strengthBonus, int dexterityBonus, int constitutionBonus, int intelligenceBonus, int wisdomBonus, int charismaBonus);
	
	public abstract int hitDiceRoll();
	
	public String getName() {
		return this.name;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public int getMaxHP() {
		return this.maxHP;
	}
	
	public int getCurrentHP() {
		return this.currentHP;
	}
	
	public int getStrength() {
		return this.strength;
	}
	
	public int getDexterity() {
		return this.dexterity;
	}
	
	public int getConstitution() {
		return this.constitution;
	}
	
	public int getIntelligence() {
		return this.intelligence;
	}
	
	public int getWisdom() {
		return this.wisdom;
	}
	
	public int getCharisma() {
		return this.charisma;
	}
	
	public ArrayList<Weapon> getWeapons() {
		return this.weapons;
	}
	
	public ArrayList<Weapon> getEquippedWeapons() {
		return this.equippedWeapons;
	}
	
	public int getProficiency() {
		if (this.level >= 17)
			return 6;
		else if (this.level >= 13 && this.level < 17)
			return 5;
		else if (this.level >= 9 && this.level < 13)
			return 4;
		else if (this.level >= 5 && this.level < 9)
			return 3;
		else
			return 2;
	}
	
	protected int getStrengthModifier() {
		return getModifier(this.strength);
	}
	
	protected int getDexterityModifier() {
		return getModifier(this.dexterity);
	}
	
	protected int getConstitutionModifier() {
		return getModifier(this.constitution);
	}
	
	public int getIntelligenceModifier() {
		return getModifier(this.intelligence);
	}
	
	protected int getWisdomModifier() {
		return getModifier(this.wisdom);
	}
	
	protected int getCharismaModifier() {
		return getModifier(this.charisma);
	}
	
	private int getModifier(int value) {
		if (value == 1)
			return -5;
		else if (value == 2 || value == 3)
			return -4;
		else if (value == 4 || value == 5)
			return -3;
		else if (value == 6 || value == 7)
			return -2;
		else if (value == 8 || value == 9)
			return -1;
		else if (value == 10 || value == 11)
			return 0;
		else if (value == 12 || value == 13)
			return 1;
		else if (value == 14 || value == 15)
			return 2;
		else if (value == 16 || value == 17)
			return 3;
		else if (value == 18 || value == 19)
			return 4;
		else
			return 5;		
	}
	
	protected void incrementLevel() {
		this.level++;
		this.loggerService.log(this.name + "'s leveled up: " + (this.level - 1) + " => " + this.level + '.');
	}
	
	protected void applyMaxHPBonus(int constitutionBonus, int hitDice) {
		int value = this.maxHP - ((this.constitution)*this.level) +  ((this.constitution + constitutionBonus) * this.level) + (hitDice * (constitutionBonus + this.constitution));
		this.maxHP += value;
		this.loggerService.log(this.name + "'s maxHP: " + (this.maxHP - value) + " => " + this.maxHP + '.');
	}
	
	protected void setEquippedWeapons(ArrayList<Weapon> weapons) {
		this.equippedWeapons = weapons;
	}
	
	protected void applyStrengthBonus(int value) {
		this.strength += value;
		this.loggerService.log(this.name + "'s strength: " + (this.strength - value) + " => " + this.strength + '.');
	}
	
	protected void applyDexterityBonus(int value) {
		this.dexterity += value;
		this.loggerService.log(this.name + "'s dexterity: " + (this.dexterity - value) + " => " + this.dexterity + '.');
	}
	
	protected void applyConstitutionBonus(int value) {
		this.constitution += value;
		this.loggerService.log(this.name + "'s constitution: " + (this.constitution - value) + " => " + this.constitution + '.');
	}
	
	protected void applyIntelligenceBonus(int value) {
		this.intelligence += value;
		this.loggerService.log(this.name + "'s intelligence: " + (this.intelligence - value) + " => " + this.intelligence + '.');
	}
	
	protected void applyWisdomBonus(int value) {
		this.wisdom += value;
		this.loggerService.log(this.name + "'s wisdom: " + (this.wisdom - value) + " => " + this.wisdom + '.');
	}
	
	protected void applyCharismaBonus(int value) {
		this.charisma += value;
		this.loggerService.log(this.name + "'s charisma: " + (this.charisma - value) + " => " + this.charisma + '.');
	}
	
}
