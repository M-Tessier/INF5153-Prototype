package Domain;

import Domain.spell.ISpell;
import java.util.ArrayList;

public class Wizard extends Character {

    public int maxLevel1SpellSlots;
    public int currentLevel1SpellSlots;

    public int maxLevel2SpellSlots;
    public int currentLevel2SpellSlots;

    public int maxLevel3SpellSlots;
    public int currentLevel3SpellSlots;

    public boolean armoured;

    public Wizard(String name, int level, int maxHP, int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma) {
        super(name, level, maxHP, strength, dexterity, constitution, intelligence, wisdom, charisma);

        //Copy-pasted code, We need to find a better place to put it. Same as setSpellSlots().
        if (level >= 13) {
            this.maxLevel1SpellSlots = 6;
            this.maxLevel2SpellSlots = 6;
            this.maxLevel3SpellSlots = 4;
        } else if (level >= 9 && level < 13) {
            this.maxLevel1SpellSlots = 6;
            this.maxLevel2SpellSlots = 4;
            this.maxLevel3SpellSlots = 2;
        } else if (level >= 5 && level < 9) {
            this.maxLevel1SpellSlots = 3;
            this.maxLevel2SpellSlots = 4;
            this.maxLevel3SpellSlots = 0;
        } else {
            this.maxLevel1SpellSlots = 2;
            this.maxLevel2SpellSlots = 0;
            this.maxLevel3SpellSlots = 0;
        }

        armoured = false;
    }

    public boolean equipWeapon(Weapon weapon) {
        return this.equipWeapon(weapon, null);
    }

    public boolean equipWeapon(Weapon weapon1, Weapon weapon2) {

        ArrayList<Weapon> weapons = new ArrayList<Weapon>();

        if ((weapon1.getWeaponType() == WeaponType.Dagger || weapon1.getWeaponType() == WeaponType.ThrowingDagger || weapon1.getWeaponType() == WeaponType.Mace)
                && (weapon2 == null || weapon2.getWeaponType() == WeaponType.Dagger || weapon2.getWeaponType() == WeaponType.ThrowingDagger || weapon2.getWeaponType() == WeaponType.Mace)) {
            if (weapon2 == null) {
                weapons.add(weapon1);
                this.loggerService.log(this.name + " equipped " + weapon1.getWeaponType().name() + '.');
            } else {
                weapons.add(weapon1);
                weapons.add(weapon2);
                this.loggerService.log(this.name + " equipped " + weapon1.getWeaponType().name() + " and the " + weapon2.getWeaponType().name() + '.');
            }

            this.setEquippedWeapons(weapons);
            return true;
        } else {
            this.loggerService.log(this.name + ", as a wizard, can only equip Daggers or Maces.");
            return false;
        }
    }

    public void levelUp(int strengthBonus, int dexterityBonus, int constitutionBonus, int intelligenceBonus, int wisdomBonus, int charismaBonus) {
        this.incrementLevel();
        this.applyMaxHPBonus(constitutionBonus, this.hitDiceRoll());
        this.applyStrengthBonus(strengthBonus);
        this.applyDexterityBonus(dexterityBonus);
        this.applyConstitutionBonus(constitutionBonus);
        this.applyIntelligenceBonus(intelligenceBonus);
        this.applyWisdomBonus(wisdomBonus);
        this.applyCharismaBonus(charismaBonus);
        this.setSpellSlots();
        this.receiveHeal(diceService.Roll(4, 4) + 4);
    }

    public int hitDiceRoll() {
        return this.diceService.Roll(6, 1);
    }

    @Override
    public int getAC() {
        if (armoured) {
            return 13 + this.getDexterity();
        } else {
            return super.getAC();
        }
    }

    private void setSpellSlots() {

        int oldLevel1SpellSlots = this.maxLevel1SpellSlots;
        int oldLevel2SpellSlots = this.maxLevel2SpellSlots;
        int oldLevel3SpellSlots = this.maxLevel3SpellSlots;

        if (this.level >= 13) {
            this.maxLevel1SpellSlots = 6;
            this.maxLevel2SpellSlots = 6;
            this.maxLevel3SpellSlots = 4;
        } else if (this.level >= 9 && this.level < 13) {
            this.maxLevel1SpellSlots = 6;
            this.maxLevel2SpellSlots = 4;
            this.maxLevel3SpellSlots = 2;
        } else if (this.level >= 5 && this.level < 9) {
            this.maxLevel1SpellSlots = 3;
            this.maxLevel2SpellSlots = 4;
            this.maxLevel3SpellSlots = 0;
        } else {
            this.maxLevel1SpellSlots = 2;
            this.maxLevel2SpellSlots = 0;
            this.maxLevel3SpellSlots = 0;
        }

        this.loggerService.log(this.name + "'s level 1 spell slots: " + oldLevel1SpellSlots + " => " + this.maxLevel1SpellSlots + '.');
        this.loggerService.log(this.name + "'s level 2 spell slots: " + oldLevel2SpellSlots + " => " + this.maxLevel2SpellSlots + '.');
        this.loggerService.log(this.name + "'s level 3 spell slots: " + oldLevel3SpellSlots + " => " + this.maxLevel3SpellSlots + '.');
    }

    public boolean castSpell(ISpell spell) {
        if (!canCast(spell)) {
            return false;
        }
        spell.cast();
        if (spell.target != null && spell.target.currentHP <= 0) {
            if (++experience == 2) {
                experience = 0;
                this.levelUp(0, 0, 0, 0, 0, 0);
            }
        }
        if (spell.level == 1) {
            this.currentLevel1SpellSlots--;
        }
        if (spell.level == 2) {
            this.currentLevel2SpellSlots--;
        }
        if (spell.level == 3) {
            this.currentLevel3SpellSlots--;
        }

        return true;
    }

    public boolean canCast(ISpell spell) {
        if (spell.level == 1) {
            return this.currentLevel1SpellSlots > 0;
        }
        if (spell.level == 2) {
            return this.currentLevel2SpellSlots > 0;
        }
        if (spell.level == 3) {
            return this.currentLevel3SpellSlots > 0;
        }
        return true;
    }
}
