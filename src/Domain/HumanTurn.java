/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import Domain.spell.AOESpell;
import Domain.spell.ISpell;
import java.util.List;

/**
 *
 * @author A-tar
 */
public class HumanTurn extends ITurn {

    protected int actionPoint;
    protected boolean completed;
    protected List<Character> enemies;
    private Controller querry = Controller.getInstance();

    protected enum Action {
        MOVE, ATTACK, CAST_SPELL, END_TURN, CHANGE_WEAPON
    }
    private Action currentAction;
    private boolean hasAttacked;
    private HumanTurn turn;

    public HumanTurn(Character _fighter, List<Character> _enemies) {
        super(_fighter);
        actionPoint = 2;
        enemies = _enemies;
    }

    @Override
    public void turn() {
        if (this.fighter.isBinded) {
            return;
        }
        getTypeofTurn();
        while (!turn.completed) {
            querry.GameScreenController(this.fighter, enemies, turn.actionPoint);
            currentAction = querry.getNextAction();
            switch (currentAction) {
                case MOVE:
                    Position p = querry.querryPosition();
                    if (turn.move(p)) {
                        this.decreaseAP();
                    } 
                    break;
                case ATTACK:
                    Character c = querry.querryTarget(this.fighter);
                    if (turn.attack(c)) {
                        this.decreaseAP();
                    }
                    break;
                case CHANGE_WEAPON:
                    Weapon[] w = querry.querryWeapons();
                    if (w[1] == null) {
                        this.fighter.equipWeapon(w[0]);
                    } else {
                        this.fighter.equipWeapon(w[0], w[1]);
                    }
                    this.decreaseAP();
                    break;
                case CAST_SPELL:
                    if (!(fighter instanceof Wizard)
                            || ArmorType.NoArmor != fighter.armor.getArmorType()
                            || fighter.equippedWeapons.size() > 1) {
                        querry.displayCantSpell();
                        break;
                    }
                    ISpell spell = querry.querrySpell(this.fighter);
                    //Character targ = querry.querryTarget(this.fighter);
                    spell.caster = fighter;
                    //spell.target = targ;
                    if (spell instanceof AOESpell)
                        ((AOESpell)spell).fighters = this.enemies;
                        
                    ((Wizard) fighter).castSpell(spell);
                    break;
                case END_TURN:
                    turn.completed = true;
                    break;
            }
        }
    }

    protected boolean move(Position p) {
        return this.fighter.move(p);
    }

    protected boolean attack(Character target) {
        if(hasAttacked)
            return false;
        Weapon equippedWeapon = null;
        for (Weapon weapon : fighter.getEquippedWeapons()) {
            equippedWeapon = weapon;
        }
        hasAttacked = true;
        return fighter.attack(target, equippedWeapon);
    }

    protected void decreaseAP() {
        turn.actionPoint--;
        if (turn.actionPoint <= 0) {
            turn.completed = true;
        }
    }

    private void getTypeofTurn() {
        if (fighter instanceof Fighter) {
            turn = new FighterTurn(fighter, enemies);
        }
        if (fighter instanceof Wizard) {
            turn = new WizardTurn(fighter, enemies);
        }
        if (fighter instanceof Rogue) {
            turn = new RogueTurn(fighter, enemies);
        }
    }

}
