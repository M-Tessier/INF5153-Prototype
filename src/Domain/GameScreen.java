/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author A-tar
 */
public class GameScreen extends UI{
    
    List<Character> fighters;
    Character currentFighter;
    int actionPoint;
    int etape = 0;
    
    public void display() {
            this.clearInterface();
             System.out.println();
            System.out.println(currentFighter.name + "'s turn (" + currentFighter.getPosition().X() + ", " + currentFighter.getPosition().Y() + ") " + actionPoint + " actions restantes :");
            displayCharacters();
    }
    
    public void updateState(Character _currentFighter, List<Character> _fighter, int _actionPoint) {
        fighters = _fighter;
        currentFighter = _currentFighter;
        actionPoint = _actionPoint;
    }
    
    public String getInput() {
        Scanner s = new Scanner(System.in);
        String x = s.nextLine();
        return x;
        
    }

    public void displayChoice() {
        System.out.println(); 
        System.out.println("What do you want to do?");
        System.out.println("1 - Move");
        System.out.println("2 - Attack");
        System.out.println("3 - Change weapon");
        System.out.println("4 - End turn");
    }
    
    public void displayMultipleWeapon() {
        System.out.println();        
        System.out.println("Equip a second weapon?");
        System.out.println("1 - Yes");
        System.out.println("2 - No");
    }
    
    public void displayAttackChoice() {
        System.out.println(); 
        System.out.println("What do you want to do?");
        System.out.println("1 - Attack");
        System.out.println("2 - Cast Spell");
    }
    
    public void displaySpells() {
        System.out.println(); 
        System.out.println("Which spell do you want to cast?");
        System.out.println("1 - Fire Bolt");
        System.out.println("2 - Magic Missile");
        System.out.println("3 - Mage Armor");
        System.out.println("4 - Misty Step");
        System.out.println("5 - Hold Person");
        System.out.println("6 - Fireball");
        System.out.println("7 - Lightning Bolt");
    }
    
    public void displayWeapons() {
        System.out.println(); 
        int i = 1;
        System.out.println("Which weapon do you want to use?"); 
        for (Weapon weap: currentFighter.weapons) {
            System.out.println(i++ + " - " + weap.weaponType);   
        }
    }
    
    public void displayImpossible() {
        System.out.println(); 
        System.out.println("This character can't cast spells");
    }
    
    public void displayTargets(Character _fighter) {
        System.out.println(); 
        int i = 1;
        System.out.println("Select the target");   
        for (Character fighter: fighters) {
            System.out.println(i++ + " - " + fighter.name + "(" + fighter.getPosition().X() + ", " + fighter.getPosition().Y() + ")");   
        }
    }
    
    public void displayMove() {
        System.out.println(); 
        Position p = this.currentFighter.getPosition();
        System.out.println("Please enter coordinate XXX YYY. Current coordinate: " + p.X() + " " + p.Y());
    }
    
    private void displayCharacters() {
        System.out.println(); 
        for (Character fighter: fighters) {
            System.out.println(fighter.name + " is at (" + fighter.getPosition().X() + ", " + fighter.getPosition().Y() + ")");   
        }
    }
}
