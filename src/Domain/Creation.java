package Domain;

import Services.DiceService;
import java.util.Scanner;


public class Creation {
	
	//TODO return instanciation de classe
	public static Character creationChar() {
		
		DiceService diceService = DiceService.getInstance();
		String stat[] = {"Strength","Dexterity","Constitution","Intelligence","Wisdom","Charisma"};
		int charStat[] = new int[6];
		int classChoice = 0;
		int nbStats = 6;		
		Scanner input = new Scanner(System.in);
		String name;
		int diceRollsForStat[] = new int[nbStats];
		
		//name choice
		System.out.println("Enter the name of your character :");
		name = input.nextLine();
		
		//class choice
		System.out.println("Enter the number of the class you would like to play :\n1-Rogue\n2-Wizard\n3-Fighter");
		while(classChoice != 1 && classChoice != 2 && classChoice !=3) {
			if(input.hasNextInt()) {
				classChoice = input.nextInt();
				if(classChoice < 1 || classChoice > 3) {
					System.out.println("Enter 1,2 or 3 please");
				}
			}else {
				System.out.println("Enter 1,2 or 3 please");
				input.next();
				continue;
			}
		}
		
		System.out.println();
		
		//stats attribution
		System.out.println("We are now gonna roll dices for " + name + " stats \nHere are the rolls : ");
		for(int i = 0; i < nbStats; i++) {
			diceRollsForStat[i] = diceRolls();
			System.out.print(diceRollsForStat[i] + " ");
		}
		
		System.out.println();
		
		for(int i = 0; i<nbStats; i++) {
			System.out.println("Enter the value you want for " + stat[i]);
			while(charStat[i] < 3 || charStat[i]  >24) {
				if(input.hasNextInt()) {
					charStat[i] = input.nextInt();
					
					//verif value in diceRollsForStats
					if(!containValue(charStat[i], diceRollsForStat)) {
						System.out.println("Enter one of the dice rolls values please");
						charStat[i] = 0;
					}else {
						int index = containValueIndex(charStat[i], diceRollsForStat, nbStats);
						diceRollsForStat[index] = 0;
					}
				}else {
					System.out.println("Enter one of the dice rolls values please");
					input.next();
				}
			}
		}	
		
		System.out.println("Here are the stats for " + name );
		for(int i = 0; i < nbStats; i++) {
			System.out.println(stat[i] + " " +charStat[i]);
		}
		
		
		//instanctiation of the character
		int maxHp = 2 + diceService.Roll(8,1) + charStat[2];
		Character character = null;
		
		if(classChoice == 1) {
			character = new Rogue(name, 1, maxHp,charStat[0], charStat[1], charStat[2], charStat[3],charStat[4],charStat[5]);
		}else if(classChoice == 2) {
			character = new Wizard(name, 1, maxHp,charStat[0], charStat[1], charStat[2], charStat[3],charStat[4],charStat[5]);
		}else if(classChoice == 3) {
			character = new Fighter(name, 1, maxHp,charStat[0], charStat[1], charStat[2], charStat[3],charStat[4],charStat[5]);
		}
		
		//input.close();
		return character;
	}

	static int diceRolls() {
		
		DiceService diceService = DiceService.getInstance();
		
		int diceRoll = 0, nbRoll = 4,min = 10;
		int rolls[] = new int[nbRoll];
		
		for(int x = 0; x<nbRoll; x++) {
			rolls[x] = diceService.Roll(8, 1);
		}
		
		for(int x = 0; x<nbRoll; x++) {
			if(rolls[x] < min) {
				min = rolls[x];
			}
		}
		
		for(int x = 0; x<nbRoll; x++) {
			diceRoll += rolls[x];
		}
		
		return diceRoll - min;
	}
	
	private static int containValueIndex(int nb, int[] diceRollsForStat, int nbStats) {
		int index = 0;
		for(int i = 0; i<nbStats; i++) {
			if(diceRollsForStat[i] == nb) {
				index = i;
				break;
			}
		}
		return index;
	}
	
	static boolean containValue(int nb, int[] array) {
		boolean contain = false;
		for(int i : array) {
			if(i == nb) {
				contain = true;
				break;
			}
		}
		return contain;
	}
}
