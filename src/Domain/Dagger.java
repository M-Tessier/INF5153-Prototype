package Domain;

public class Dagger extends Weapon {

	public Dagger() {
		super();
		this.weaponType = WeaponType.Dagger;
		this.isTwoHanded = false;
		this.range = 5;
	}
	
	public int getDamage(Character owner, boolean critical) {
		int value = this.diceService.Roll(4, 1);
		if (critical)
			value *= 2;
		
		return value + owner.getStrengthModifier();
	}
}
